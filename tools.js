/*	tools.js							*/
/*  Copyright Snowite 2014              */
/*	----------------------------------  */

/*	$Slugify							*/
/*	$Random urlId 						*/
/*	$Html Encode 						*/
/*	----------------------------------  */


module.exports = {
/*	$Slugify							*/
/*	----------------------------------  */
	slug: function(str) {
		st = str.toLowerCase();
		st = st.replace(new RegExp(/[àáâãäå]/g),"a");
		st = st.replace(new RegExp(/æ/g),"ae");
		st = st.replace(new RegExp(/ç/g),"c");
		st = st.replace(new RegExp(/[èéêë]/g),"e");
		st = st.replace(new RegExp(/[ìíîï]/g),"i");
		st = st.replace(new RegExp(/ñ/g),"n");
		st = st.replace(new RegExp(/[òóôõö]/g),"o");
		st = st.replace(new RegExp(/œ/g),"oe");
		st = st.replace(new RegExp(/[ùúûü]/g),"u");
		st = st.replace(new RegExp(/[ýÿ]/g),"y");
		st = st.replace(/[^a-z0-9 ]+/gi,'')
		st = st.trim().replace(/ /g,'-');
		st = st.replace(/[\-]{2}/g,'');
		st.replace(/[^a-z\- ]*/gi,'');
		return (st);
	},
/*	$Random urlId 						*/
/*	----------------------------------  */
	urlid: function() {
		var urlId = "";
		var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < 5; i++ )
		    urlId += possible.charAt(Math.floor(Math.random() * possible.length));
		return urlId;
	},
/*	$Html Encode 						*/
/*	----------------------------------  */
	htmlencode: function(str) {
		if(typeof(str)!="string") {
			str=str.toString();
		};
		str=str.replace(/&/g, "&amp;") ;
		str=str.replace(/"/g, "&quot;") ;
		str=str.replace(/</g, "&lt;") ;
		str=str.replace(/>/g, "&gt;") ;
		str=str.replace(/'/g, "&#146;") ;
		return str;
	}
}
